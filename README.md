# Star Roll
## Téléchargement/Download :
https://frama.link/StarRoll_W (win)
https://frama.link/StarRoll_W32 (win32)
https://frama.link/StarRoll_L (linux)
https://frama.link/StarRoll_L32 (linux32)

## Description:
[EN]
Inputs:
Use left click to forward and right click to back on the path.
Don't be touch by meteors or burning walls !
Press escape -in level): to quit the level. Press escape (in main menu): to quit the game.

Hello explorers!
We are two developpers from France, and decided to take the challenge to do a game on a very Low rez (in a little less than two days!)
This game is about a space cinnamon roll which wants to be the prettiest cinnamon roll by collecting star to decorate itself.
The space is full of danger for a cute cake, and your mission, if you accept it, is to guide it throught

[FR]
Touches :
Le clic gauche permet d'avancer d'une case sur le chemin, le clic droit permet de reculer d'une case.
Ne vous faites pas toucher par les météorites et faites attention aux murs devenus brûlants par les impacts de météorites !
Appuyer sur echap pour quitter le niveau en cours. Appuyer sur echap (dans le menu principal) pour quitter le jeu.

Salut exploratrice·eurs de l'espace !
Nous sommes deux développeuse·rs français·e et nous avons décidé de participer à la LowRezJam 2017 tenu sur itch.io. Il s'agissait de réaliser un jeu vidéo avec un rendu 64x64 (en un peu moins de deux jours).
Dans ce jeu, vous suivez un gâteau de l'espace qui veut devenir le plus beau des gâteaux en collectant des étoiles pour se décorer.
L'espace est cependant plein de dangers pour un gâteau aussi mignon et votre mission, si vous l'acceptez, est de guider ce dernier à travers tous ces dangers.
***

## Instructions:
[EN] 
You can download only the .unitypackage file or fork the project. I add both for those who just want to see sources without download anything.

[FR]
Vous pouvez télécharger uniquement le fichier .unitypackage ou bien fork le projet. J'ai mis les deux à disposition pour celleux qui souhaitent ne regarder que les sources sans rien télécharger.

## Credits :
### Unity Assets:
* Vast Outer Space (by Prodigious Creations) : https://www.assetstore.unity3d.com/en/#!/content/38913

### Songs:
* [MusicMenu] Rndm Loop 12 (by Bas3008) : http://www.newgrounds.com/audio/listen/692926 
* [MusicLevel] Low Danger (by Vladimistar) http://www.newgrounds.com/audio/listen/720999
* Evening Road (by TwelfthChromatic) : http://www.newgrounds.com/audio/listen/551162

### SFX:
* [DeathSFX] explosion flangered (by destro_94) : https://freesound.org/people/destro_94/sounds/84521/
* [SpawnSFX] Explosion (by Aiwha) : https://freesound.org/people/Aiwha/sounds/250712/

### Fonts:
* joystix Font (by Raymond Larabie) : http://www.1001fonts.com/joystix-font.html#styles
* Shablagoo (by Iconian Fonts) http://www.dafont.com/fr/shablagoo.font
***

## Notes de version
### 1.2 : 
#### Ajouts :
- Un niveau 3 a été ajouté

#### Corrections de bug :
- Il n'était pas possible de passer du niveau 2 au niveau 3
- Une des faces du niveau 2 n'était pas correctement liée.

### 1.1 : 
#### Ajouts :
- Un niveau 2 a été ajouté

#### Modifications :
- La vitesse des météores a été augmenté de 25 à 40.
- L'écran de fin sur les crédits a été modifié
- La génération de météores est suspendue lorsque le jeu est en pause

#### Corrections de bug :
- Quand on reculait sur la première case du niveau, le jeu était bloqué en mode pause
- Les collisions avec les météores ont été corrigé.
- Les collisions avec les murs en fusion ont été corrigé.
- Lorsque l'on change de face, les météores déjà générés disparaissent
- Le système de rotation a été corrigé pour tous les cas utilisés
