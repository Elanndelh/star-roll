v1.2 : 
Ajouts :
- Un niveau 3 a �t� ajout�

Corrections de bug :
- Il n'�tait pas possible de passer du niveau 2 au niveau 3
- Une des faces du niveau 2 n'�tait pas correctement li�e.

v1.1 :
Ajouts :
- Un niveau 2 a �t� ajout�

Modifications :
- La vitesse des m�t�ores a �t� augment� de 25 � 40.
- L'�cran de fin sur les cr�dits a �t� modifi�
- La g�n�ration de m�t�ores est suspendue lorsque le jeu est en pause

Corrections de bug :
- Quand on reculait sur la premi�re case du niveau, le jeu �tait bloqu� en mode pause
- Les collisions avec les m�t�ores ont �t� corrig�.
- Les collisions avec les murs en fusion ont �t� corrig�.
- Lorsque l'on change de face, les m�t�ores d�j� g�n�r�s disparaissent
- Le syst�me de rotation a �t� corrig� pour tous les cas utilis�s
