﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LowRezJam2017
{
    public class WallBehavior : MonoBehaviour
    {
        bool _isFront;

        [Tooltip("Duration of burning when a meteor hit the wall")]
        public float burnDuration = 2.5f;
        float _currentBurningDuration;
        bool _isBurning;
        public bool IsBurning() { return _isBurning; }

        Animator _animator; //1/ Not used (Keep it if you want a animation during burning)
        Renderer _renderer;
        CubeManager _cubeManager;


    ///////////////////////////////////////////////////////////////
    /// GENERAL FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        void Awake()
        {
            _cubeManager = CubeManager.Instance;

            _renderer = GetComponent<Renderer>();
            if (_renderer == null)
                Debug.LogError(gameObject.name + " : _renderer is null.");
        }
        /*********************************************************/

        void Start()
        {
            /*1/_animator = GetComponent<Animator>();
            if (_animator == null)
                Debug.LogError(gameObject.name + " : _animator is null.");*/
        }
        /*********************************************************/

        void Update()
        {
            if (_isBurning)
            {
                UpdateBurning();
            }
        }
        /*********************************************************/

        void OnTriggerEnter(Collider a_other)
        {
            if (a_other.gameObject.GetComponent<Meteor>() != null)
            {
                InitializeBurning();
            }
        }
        /*********************************************************/
        
    ///////////////////////////////////////////////////////////////
    /// PRIVATE FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        /// <summary>
        /// Initialize the burning
        /// </summary>
        void InitializeBurning()
        {
            _isBurning = true;
            _currentBurningDuration = 0f;
            /*1/if (_animator != null)
                _animator.SetTrigger("StartBurning");*/
            _renderer.material = _cubeManager.GetDamageMaterial();
        }
        /*********************************************************/

        /// <summary>
        /// Increase _currentBurningDuration by deltaTime.
        /// When the burnDuration is reached, stop the burning
        /// </summary>
        void UpdateBurning()
        {
            _currentBurningDuration += Time.deltaTime;
            if (_currentBurningDuration > burnDuration)
            {
                _currentBurningDuration = 0f;
                _isBurning = false;
                /*1/if (_animator != null)
                    _animator.SetTrigger("StopBurning");*/
                SetCurrentMaterial();
            }
        }
        /*********************************************************/

        /// <summary>
        /// Set current material depending on _isFront
        /// </summary>
        void SetCurrentMaterial()
        {
            if (_isFront)
                _renderer.material = _cubeManager.GetFrontMaterial();
            else
                _renderer.material = _cubeManager.GetBackgroundMaterial();
        }
        /*********************************************************/
        
    ///////////////////////////////////////////////////////////////
    /// PUBLIC FUNCTIONS //////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        public void SetFront(bool a_isFront)
        {
            _isFront = a_isFront;
            SetCurrentMaterial();
        }
        /*********************************************************/
    }
}
