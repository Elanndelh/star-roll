﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LowRezJam2017
{
    public class PlayerBehaviour : MonoBehaviour
    {
        //public float speed = 1.0f;
        [SerializeField] [Tooltip("Unspawn VFX")]
        ParticleSystem _unspawnFX;
        [SerializeField] [Tooltip("Spawn VFX")]
        ParticleSystem _spawnFX;
        [SerializeField] [Tooltip("Death VFX")]
        ParticleSystem _deathFX;
        [SerializeField] [Tooltip("Spawn SFX")]
        AudioClip _spawnSFX; //1/ Not used
        [SerializeField] [Tooltip("Death SFX")]
        AudioClip _deathSFX; //1/ Not used

        CubeManager _cubeManager;
        Animator _animator;
        AudioSource _audioSource; //1/ Not used


    ///////////////////////////////////////////////////////////////
    /// GENERAL FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        void Awake()
        {
            // Check compulsory fields
            Debug.Assert(_unspawnFX != null, gameObject.name + " : _unspawnFX cannot be null");
            Debug.Assert(_spawnFX != null, gameObject.name + " : _spawnFX cannot be null");
            Debug.Assert(_deathFX != null, gameObject.name + " : _deathFX cannot be null");
            //1/Debug.Assert(_spawnSFX != null, gameObject.name + " : _spawnSFX cannot be null");
            //1/Debug.Assert(_deathSFX != null, gameObject.name + " : _deathSFX cannot be null");
        }
        /*********************************************************/

        void Start()
        {
            _cubeManager = CubeManager.Instance;

            _animator = GetComponent<Animator>();
            if (_animator == null)
                Debug.LogError(gameObject.name + " : _animator is null.");

            /*1/_audioSource = GetComponent<AudioSource>();
            if (_audioSource == null)
                Debug.LogError(gameObject.name + " : _audioSource is null.");*/

            GetComponentInChildren<PlayerMeteorCollision>().onMeteorCollision += MeteorCollision;
        }
        /*********************************************************/

        void Update()
        {
            if (LevelManager.Instance.IsPaused() == false)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    MoveForward();
                }
                else if (Input.GetMouseButtonDown(1))
                {
                    MoveBack();
                }
            }
        }
        /*********************************************************/

        void OnCollisionEnter(Collision a_collision)
        {
            if (a_collision.gameObject.name == "Star")
            {
                FinishLevel();
            }
            else if (a_collision.gameObject.GetComponent<Meteor>() != null)
            {
                _animator.SetTrigger("Death");
                _deathFX.Play();
                /*1/_audioSource.clip = _deathSFX;
                _audioSource.Play();*/
                LevelManager.Instance.GameOver();
            }
        }
        /*********************************************************/

        void OnTriggerStay(Collider a_other)
        {
            if (a_other.gameObject.GetComponent<WallBehavior>() != null)
            {
                if (a_other.gameObject.GetComponent<WallBehavior>().IsBurning())
                {
                    _animator.SetTrigger("Death");
                    _deathFX.Play();
                    /*1/_audioSource.clip = _deathSFX;
                    _audioSource.Play();*/
                    LevelManager.Instance.GameOver();
                }
            }
        }
        /*********************************************************/

     ///////////////////////////////////////////////////////////////
     /// PRIVATE FUNCTIONS /////////////////////////////////////////
     ///////////////////////////////////////////////////////////////
         /// <summary>
         /// Called to move forward the player in the path.
         /// </summary>
         void MoveForward()
         {
             GameObject nextGameObjectPath = _cubeManager.MoveForward();
             if (nextGameObjectPath != null)
                 transform.position = nextGameObjectPath.transform.position;
         }
         /*********************************************************/

        /// <summary>
        /// Called to move back the player in the path.
        /// </summary>
        void MoveBack()
        {
            GameObject previousGameObjectPath = _cubeManager.MoveBack();
            if (previousGameObjectPath != null)
                transform.position = previousGameObjectPath.transform.position;
        }
        /*********************************************************/

        /// <summary>
        /// Called when the level is finished.
        /// Load the next level.
        /// </summary>
        void FinishLevel()
        {
            Debug.Log("Level finish!");
            LevelManager.Instance.LoadNextLevel();
        }
        /*********************************************************/

        /// <summary>
        /// Called when a meteor hurts the player
        /// </summary>
        void MeteorCollision()
        {
            _animator.SetTrigger("Death");
            _deathFX.Play();
            /*1/_audioSource.clip = _deathSFX;
            _audioSource.Play();*/
             LevelManager.Instance.GameOver();
        }
        /*********************************************************/
        
    ///////////////////////////////////////////////////////////////
    /// PUBLIC FUNCTIONS //////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        /// <summary>
        /// Called to play spawn FX
        /// </summary>
        public void PlaySpawnFX()
        {
            _spawnFX.Play();
            /*1/_audioSource.clip = _spawnSFX;
            _audioSource.Play();*/
        }
        /*********************************************************/
        
        /// <summary>
        /// Called to play unspawn FX
        /// </summary>
        public void PlayUnspawnFX()
        {
            _unspawnFX.Play();
            /*1/_audioSource.clip = _spawnSFX;
            _audioSource.Play();*/
        }
        /*********************************************************/
    }
}
