﻿using System;
using UnityEngine;

namespace LowRezJam2017
{
    public class PlayerMeteorCollision : MonoBehaviour
    {
        public Action onMeteorCollision;

        
    ///////////////////////////////////////////////////////////////
    /// GENERAL FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        void OnTriggerEnter(Collider a_other)
        {
            if (a_other.gameObject.GetComponent<Meteor>() != null)
            {
                if (onMeteorCollision != null)
                    onMeteorCollision();
            }
        }
        /*********************************************************/
    }
}
