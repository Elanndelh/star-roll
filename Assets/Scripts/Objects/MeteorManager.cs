﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LowRezJam2017
{
    public class MeteorManager : MonoBehaviour
    {
        [SerializeField] [Tooltip("The FaceBehaviour in front of gameObject at Awake")]
        FaceBehaviour _face;

        [SerializeField] [Tooltip("A list of several standard meteor's prefabs")]
        List<GameObject> _meteorPrefabs;
        //List<GameObject> _currentMeteors;

        [SerializeField] [Tooltip("A list of several big meteor's prefabs")]
        List<GameObject> _bigMeteorPrefabs;
        //List<GameObject> _currentBigMeteors;

        [SerializeField] [Tooltip("The speed of meteors.")]
        float _meteorSpeed = 40f;

        [SerializeField] [Tooltip("The ShadowDirection to place correctly shadows.")]
        ShadowDirection _shadowDirection;
        Vector3 _direction;

        CubeManager _cubeManager;

        [SerializeField] [Tooltip("Standard duration between two standard meteors.")]
        float c_durationBetweenStandardMeteors = 10f;
        [SerializeField] [Tooltip("Standard duration between two big meteors.")]
        float c_durationBetweenBigMeteors = 100f;
        float _currentDurationBetweenStandardMeteors;
        float _currentDurationBetweenBigMeteors;
        [Range(0, 1)] [Tooltip("Delta max what influence currentDurations between meteors")]
        public float _durationDeltaMax = 0.1f;
        float _currentDeltaTimeStandard;
        float _currentDeltaTimeBig;


    ///////////////////////////////////////////////////////////////
    /// GENERAL FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        void Awake()
        {
            // Check compulsory fields
            Debug.Assert(_face != null, gameObject.name + " : _face cannot be null");
            Debug.Assert(_meteorPrefabs != null, gameObject.name + " : _meteorPrefabs cannot be null");
            Debug.Assert(_bigMeteorPrefabs != null, gameObject.name + " : _bigMeteorPrefabs cannot be null");
        }
        /*********************************************************/

        void Start()
        {
            _cubeManager = CubeManager.Instance;

            _currentDeltaTimeStandard = 0f;
            _currentDeltaTimeBig = 0f;
            _currentDurationBetweenStandardMeteors = c_durationBetweenStandardMeteors + c_durationBetweenStandardMeteors * Random.Range(0, _durationDeltaMax) - 0.5f * _durationDeltaMax;
            _currentDurationBetweenBigMeteors = c_durationBetweenBigMeteors + c_durationBetweenBigMeteors * Random.Range(0, _durationDeltaMax) - 0.5f * _durationDeltaMax;

            // Set _direction depending on its _shadowDirection
            switch (_shadowDirection)
            {
                case ShadowDirection.Forward:
                    _direction = Vector3.forward;
                    break;
                case ShadowDirection.Back:
                    _direction = Vector3.back;
                    break;
                case ShadowDirection.Top:
                    _direction = Vector3.up;
                    break;
                case ShadowDirection.Bottom:
                    _direction = Vector3.down;
                    break;
                case ShadowDirection.Left:
                    _direction = Vector3.back;
                    break;
                case ShadowDirection.Right:
                    _direction = Vector3.right;
                    break;
                default:
                    Debug.Log("Not a validate shadowDirection");
                    return;
            }

            _cubeManager.onPreChangeFaces += delegate { DestroyAllMeteors(); };
            _cubeManager.onChangeFaces += delegate { CheckCurrentFace(); };
            CheckCurrentFace(true);
        }
        /*********************************************************/

        void Update()
        {
            // Keep it if you want control the generation of meteors. Usefull to test
            // Test_ControlGenerationOfMeteors();

            if (LevelManager.Instance.IsPaused() == false)
                GenerateMeteors();
        }
        /*********************************************************/

    ///////////////////////////////////////////////////////////////
    /// PRIVATE FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        /// <summary>
        /// Called in Update to generate automatically meteors depending on _currentDurationBetweenBigMeteors and _currentDurationBetweenStandardMeteors
        /// </summary>
        void GenerateMeteors()
        {
            _currentDeltaTimeBig += Time.deltaTime;
            if (_currentDeltaTimeBig > _currentDurationBetweenBigMeteors)
            {
                _currentDeltaTimeBig = 0f;
                _currentDeltaTimeStandard = 0f;
                _currentDurationBetweenBigMeteors = c_durationBetweenBigMeteors + c_durationBetweenBigMeteors * Random.Range(0, _durationDeltaMax) - 0.5f * _durationDeltaMax * c_durationBetweenBigMeteors;
                InitializeBigMeteor();
            }

            _currentDeltaTimeStandard += Time.deltaTime;
            if (_currentDeltaTimeStandard > _currentDurationBetweenStandardMeteors)
            {
                _currentDeltaTimeStandard = 0f;
                _currentDurationBetweenStandardMeteors = c_durationBetweenStandardMeteors + c_durationBetweenStandardMeteors * Random.Range(0, _durationDeltaMax) - 0.5f * _durationDeltaMax * c_durationBetweenStandardMeteors;
                InitializeStandardMeteor();
            }
        }
        /*********************************************************/

        /// <summary>
        /// Called to created and initialize a new meteor with random position, random mesh and value of its MeteorManager
        /// </summary>
        /// <param name="a_numberByRow">Determine the step of grid (8 for a grid 8x8)</param>
        /// <param name="a_meteorPrefabs">List of meteor's prefabs</param>
        void InitializeMeteor(int a_numberByRow, List<GameObject> a_meteorPrefabs)
        {
            // Create a new meteor
            int rand = Random.Range(0, a_meteorPrefabs.Count);
            GameObject meteor = Instantiate(a_meteorPrefabs[rand], transform);

            // Set localPosition of this meteor
            int meteorPosX = Random.Range(0, a_numberByRow);
            int meteorPosY = Random.Range(0, a_numberByRow);
            float f_meteorX = (1f / a_numberByRow * (float)((meteorPosX - a_numberByRow/2)) + 1f / (float)(a_numberByRow * 2));
            float f_meteorY = (1f / a_numberByRow * (float)((meteorPosY - a_numberByRow/2)) + 1f / (float)(a_numberByRow * 2));
            meteor.transform.localPosition = new Vector3(f_meteorX, f_meteorY, 0.0f);

            // Initialize this meteor
            meteor.GetComponent<Meteor>().SetSpeed(_meteorSpeed);
            meteor.GetComponent<Meteor>().SetFaceManager(_face);
            meteor.GetComponent<Meteor>().SetMeteorManager(this);
            meteor.GetComponent<Meteor>().SetShadowDirection(_shadowDirection);
            meteor.GetComponent<Meteor>().InitializeShadow();
        }
        /*********************************************************/

        /// <summary>
        /// Called to created and initialize a standard meteor
        /// Call InitializeMeteor with arguments of standard meteor
        /// </summary>
        void InitializeStandardMeteor()
        {
            InitializeMeteor(8, _meteorPrefabs);
        }
        /*********************************************************/

        /// <summary>
        /// Called to created and initialize a big meteor
        /// Call InitializeMeteor with arguments of big meteor
        /// </summary>
        void InitializeBigMeteor()
        {
            InitializeMeteor(4, _bigMeteorPrefabs);
        }
        /*********************************************************/

        /// <summary>
        /// Launch CheckCurrentFaceCoroutine
        /// </summary>
        /// <param name="a_isInitialization"></param>
        void CheckCurrentFace(bool a_isInitialization = false)
        {
            StartCoroutine(CheckCurrentFaceCoroutine(a_isInitialization));
        }
        /*********************************************************/

        /// <summary>
        /// Called to check with a raycast which is the face in front of this MeteorManager
        /// </summary>
        /// <param name="a_isInitialization"></param>
        /// <returns></returns>
        IEnumerator CheckCurrentFaceCoroutine(bool a_isInitialization = false)
        {
            if (a_isInitialization)
                yield return new WaitForEndOfFrame();
            else
                yield return new WaitForSeconds(_cubeManager.rotationFaceDuration);
            
            RaycastHit hit;
            if (Physics.Raycast(transform.position, _direction, out hit))
            {
                if (hit.transform.GetComponentInParent<FaceBehaviour>() != null)
                    _face = hit.transform.GetComponentInParent<FaceBehaviour>();
            }
        }
        /*********************************************************/

        void DestroyAllMeteors()
        {
            foreach (Transform meteor in transform)
            {
                Destroy(meteor.gameObject);
            }
        }
        /*********************************************************/

        #region Tests
        /// <summary>
        /// Test function: called to generate meteor with keyboard inputs. Usefull to test.
        /// </summary>
        void Test_ControlGenerationOfMeteors()
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                InitializeStandardMeteor();
            }
            else if (Input.GetKeyDown(KeyCode.Z))
            {
                InitializeBigMeteor();
            }
        }
        /*********************************************************/
        #endregion

    ///////////////////////////////////////////////////////////////
    /// PUBLIC FUNCTIONS //////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
    }
}
