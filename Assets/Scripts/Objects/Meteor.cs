﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LowRezJam2017
{
    public enum ShadowDirection { Forward, Back, Top, Bottom, Left, Right }


    public class Meteor : MonoBehaviour
    {
        ShadowDirection _shadowDirection;
        public void SetShadowDirection(ShadowDirection a_shadowDirection) { _shadowDirection = a_shadowDirection; }

        [SerializeField] [Tooltip("Prefab of the meteor' shadow")]
        GameObject _shadowMeteorPrefab;
        GameObject _currentShadow;

        FaceBehaviour _face;
        public void SetFaceManager(FaceBehaviour a_face) { _face = a_face; }
        MeteorManager _meteorManager;
        public void SetMeteorManager(MeteorManager a_meteorManager) { _meteorManager = a_meteorManager; }
        float _speed;
        public void SetSpeed(float a_speed) { _speed = a_speed; }

        Vector3 _shadowScaleMax;


    ///////////////////////////////////////////////////////////////
    /// GENERAL FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        void Awake()
        {
            // Check compulsory fields
            Debug.Assert(_shadowMeteorPrefab != null, gameObject.name + " : _shadowMeteorPrefab cannot be null");
        }
        /*********************************************************/

        void Update()
        {
            transform.Translate(Vector3.forward * Time.deltaTime * _speed);
            UpdateShadow();
        }
        /*********************************************************/

        void OnTriggerEnter(Collider a_other)
        {
            if(a_other.GetComponent<FaceBehaviour>())
                StartCoroutine(DestroyCoroutine());
            else if (a_other.tag == "Player")
            {
                Destroy(gameObject);
            }
        }
        /*********************************************************/

        void OnDestroy()
        {
            if (_currentShadow != null)
                Destroy(_currentShadow);
        }
        /*********************************************************/
        
    ///////////////////////////////////////////////////////////////
    /// PRIVATE FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        /// <summary>
        /// Called to update the scale of meteor' shadow.
        /// </summary>
        void UpdateShadow()
        {
            float distanceBetweenMeteorManagerAndFace;
            float distanceWithFace;

            switch (_shadowDirection)
            {
                case ShadowDirection.Forward:
                    distanceBetweenMeteorManagerAndFace = Mathf.Abs(_meteorManager.transform.position.z - _face.transform.position.z);
                    distanceWithFace = Mathf.Abs(_face.transform.position.z - transform.position.z);
                    break;
                case ShadowDirection.Back:
                    distanceBetweenMeteorManagerAndFace = Mathf.Abs(_meteorManager.transform.position.z - _face.transform.position.z);
                    distanceWithFace = Mathf.Abs(_face.transform.position.z - transform.position.z);
                    break;
                case ShadowDirection.Top:
                    distanceBetweenMeteorManagerAndFace = Mathf.Abs(_meteorManager.transform.position.y - _face.transform.position.y);
                    distanceWithFace = Mathf.Abs(_face.transform.position.y - transform.position.y);
                    break;
                case ShadowDirection.Bottom:
                    distanceBetweenMeteorManagerAndFace = Mathf.Abs(_meteorManager.transform.position.y - _face.transform.position.y);
                    distanceWithFace = Mathf.Abs(_face.transform.position.y - transform.position.y);
                    break;
                case ShadowDirection.Left:
                    distanceBetweenMeteorManagerAndFace = Mathf.Abs(_meteorManager.transform.position.x - _face.transform.position.x);
                    distanceWithFace = Mathf.Abs(_face.transform.position.x - transform.position.x);
                    break;
                case ShadowDirection.Right:
                    distanceBetweenMeteorManagerAndFace = Mathf.Abs(_meteorManager.transform.position.x - _face.transform.position.x);
                    distanceWithFace = Mathf.Abs(_face.transform.position.x - transform.position.x);
                    break;
                default:
                    Debug.Log("Not a validate shadowDirection");
                    return;
            }

            float currentScaleX = _shadowScaleMax.x * (distanceBetweenMeteorManagerAndFace - distanceWithFace) / distanceBetweenMeteorManagerAndFace;
            float currentScaleY = _shadowScaleMax.y * (distanceBetweenMeteorManagerAndFace - distanceWithFace) / distanceBetweenMeteorManagerAndFace;
            float currentScaleZ = _shadowScaleMax.z * (distanceBetweenMeteorManagerAndFace - distanceWithFace) / distanceBetweenMeteorManagerAndFace;
            if (_currentShadow)
                _currentShadow.transform.localScale = new Vector3(currentScaleX, currentScaleY, currentScaleZ);
        }
        /*********************************************************/

        /// <summary>
        /// Coroutine: called to destroy gameObject and its shadow
        /// </summary>
        IEnumerator DestroyCoroutine()
        {
            Destroy(_currentShadow);
            yield return new WaitForSeconds(0.5f);
            if (gameObject)
                Destroy(gameObject);
        }
        /*********************************************************/
            
    ///////////////////////////////////////////////////////////////
    /// PUBLIC FUNCTIONS //////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        /// <summary>
        /// Called to create and initialize parameters of meteor' shadow's (position and scale)
        /// </summary>
        public void InitializeShadow()
        {
            _shadowScaleMax = _shadowMeteorPrefab.transform.localScale;
            float delta = 8f;

            Vector3 shadowPosition = transform.position;
            switch (_shadowDirection)
            {
                case ShadowDirection.Forward:
                    shadowPosition.z = _face.transform.position.z - delta;
                    break;
                case ShadowDirection.Back:
                    shadowPosition.z = _face.transform.position.z + delta;
                    break;
                case ShadowDirection.Top:
                    shadowPosition.y = _face.transform.position.y - delta;
                    break;
                case ShadowDirection.Bottom:
                    shadowPosition.y = _face.transform.position.y + delta;
                    break;
                case ShadowDirection.Left:
                    shadowPosition.x = _face.transform.position.x - delta;
                    break;
                case ShadowDirection.Right:
                    shadowPosition.x = _face.transform.position.x + delta;
                    break;
                default:
                    Debug.LogError("Not a validate shadowDirection");
                    break;
            }

            _currentShadow = Instantiate(_shadowMeteorPrefab, _face.transform);
            _currentShadow.transform.position = shadowPosition;
            _currentShadow.transform.localScale = Vector3.zero;
        }
        /*********************************************************/
    }
}
