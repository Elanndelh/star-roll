﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace LowRezJam2017
{
    public class CubeManager : MonoBehaviour
    {
        [SerializeField] [Tooltip("List of level's FaceBehaviours")]
        List<FaceBehaviour> _faces;
        FaceBehaviour _currentFace;

        [SerializeField] [Tooltip("Material for walls in frontground")]
        Material _frontMaterial;
        public Material GetFrontMaterial() { return _frontMaterial; }
        [SerializeField] [Tooltip("Material for walls in background")]
        Material _backgroundMaterial;
        public Material GetBackgroundMaterial() { return _backgroundMaterial; }
        [SerializeField] [Tooltip("Material for walls when they kill player because hit by meteor and burning")]
        Material _damageMaterial;
        public Material GetDamageMaterial() { return _damageMaterial; }

        public static CubeManager Instance;

        [Tooltip("Duration for the complete rotation when the current front Face change")]
        public float rotationFaceDuration = 3.0f;
        public Action onPreChangeFaces;
        public Action onChangeFaces;

        Quaternion _previousRotation;
        Quaternion _newRotation;

        public TargetFace testface;


    ///////////////////////////////////////////////////////////////
    /// GENERAL FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        void Awake()
        {
            Instance = this;

            // Check compulsory fields
            Debug.Assert(_faces != null, "_faces cannot be null");
            Debug.Assert(_frontMaterial != null, "_frontMaterial cannot be null");
            Debug.Assert(_backgroundMaterial != null, "_backgroundMaterial cannot be null");
            Debug.Assert(_damageMaterial != null, "_damageMaterial cannot be null");
        }
        /*********************************************************/

        void Start () {
            _currentFace = _faces[0];
            StartCoroutine(InitializePlayerCoroutine());

            ChangeWallMaterial();
        }
        /*********************************************************/

        void Update()
        {
            //   TestUpdate();
            if (Input.GetKeyDown(KeyCode.A))
                ChangeFace(testface);
        }

    ///////////////////////////////////////////////////////////////
    /// PRIVATE FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        /// <summary>
        /// Change material (front or background) of all walls in each FaceBehaviour in _faces.
        /// </summary>
        void ChangeWallMaterial()
        {
            foreach (FaceBehaviour face in _faces)
            {
                if (face != _currentFace)
                    face.SetBackgroundMaterial();
                else
                    _currentFace.SetFrontMaterial();
            }
        }
        /*********************************************************/

        /// <summary>
        /// Change the active face's player (unspawn the previous, spawn the new) in coroutine 
        /// </summary>
        /// <param name="a_newFace">[FaceBehaviour]The new FaceBehaviour</param>
        void ChangeFaceActive(FaceBehaviour a_newFace)
        {
            _currentFace.player.GetComponent<PlayerBehaviour>().PlayUnspawnFX();
            StartCoroutine(UnspawnCoroutine(a_newFace));
        }
        /*********************************************************/

        /// <summary>
        /// Coroutine : Initialize the player when the TitleImage is disappear 
        /// And play FX
        /// </summary>
        IEnumerator InitializePlayerCoroutine()
        {
            yield return new WaitForSeconds(1.5f); // Length of Hide_TitleImage (clip)
            _currentFace.player.SetActive(true);
            yield return new WaitForFixedUpdate(); // Wait to be sure that Player is created and initialize (Start()).
            _currentFace.player.GetComponent<PlayerBehaviour>().PlaySpawnFX();
        }
        /*********************************************************/

        /// <summary>
        /// Coroutine : Unspawn the player in the previous face
        /// And spawn the player in the new face + Change wall's material + resume the game
        /// </summary>
        /// <param name="a_newFace">[FaceBehaviour] The new FaceBehaviour</param>
        IEnumerator UnspawnCoroutine(FaceBehaviour a_newFace)
        {
            if (onPreChangeFaces != null)
                onPreChangeFaces();

            yield return new WaitForSeconds(rotationFaceDuration * 0.5f);
            _currentFace.player.SetActive(false);

            yield return new WaitForSeconds(rotationFaceDuration * 0.5f);
            _currentFace = a_newFace;
            _currentFace.player.SetActive(true);
            _currentFace.player.GetComponent<PlayerBehaviour>().PlaySpawnFX();

            ChangeWallMaterial();
            if (onChangeFaces != null)
                onChangeFaces();

            LevelManager.Instance.SetPaused(false);
        }
        /*********************************************************/
        
    ///////////////////////////////////////////////////////////////
    /// PUBLIC FUNCTIONS //////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        public GameObject MoveForward()
        {
            return _currentFace.MoveForward();
        }
        /*********************************************************/

        public GameObject MoveBack()
        {
            return _currentFace.MoveBack();
        }
        /*********************************************************/

        /// <summary>
        /// Pause the game + Launch a tween to rotate the CubeManager with the right angle (depend on a_targetFace).
        /// </summary>
        /// <param name="a_targetFace">[TargetFace] The new TargetFace</param>
        public void ChangeFace(TargetFace a_targetFace)
        {
            LevelManager.Instance.SetPaused(true);

            // Launch a tween to rotate the CubeManager with the right angle (depend on a_targetFace)
            Vector3 currentAngle = transform.rotation.eulerAngles + Vector3.one * 360f;
            Debug.Log(currentAngle);
            Debug.Log("##############################");
            /*switch (a_targetFace.rotationMode)
            {
                case RotationMode.None:
                    Debug.Log("none");
                    return;
                case RotationMode.Left:
                    Debug.Log("left");
                    // iTween.RotateTo(gameObject, new Vector3(currentAngle.x, currentAngle.y - 90.0f % 360f, currentAngle.z), rotationFaceDuration);
                  //  iTween.RotateTo(gameObject, new Vector3(currentAngle.x, currentAngle.y, currentAngle.z - 90.0f), rotationFaceDuration);
                    _newRotation = Quaternion.Euler(currentAngle.x + 0f, currentAngle.y + 0f, currentAngle.z - 90f);
                    break;
                case RotationMode.Right:
                    Debug.Log("right");
                    //iTween.RotateTo(gameObject, new Vector3(currentAngle.x, currentAngle.y + 90.0f % 360f, currentAngle.z), rotationFaceDuration);
                   // iTween.RotateTo(gameObject, new Vector3(currentAngle.x, currentAngle.y, currentAngle.z + 90.0f), rotationFaceDuration);
                    _newRotation = Quaternion.Euler(currentAngle.x + 0f, currentAngle.y + 0f, currentAngle.z + 90f);
                    break;
                case RotationMode.Bottom:
                    Debug.Log("bottom");
                    //iTween.RotateTo(gameObject, new Vector3(currentAngle.x + 90.0f, currentAngle.y, currentAngle.z), rotationFaceDuration);
                   // iTween.RotateTo(gameObject, iTween.Hash("x", currentAngle.x + 90f, "time", rotationFaceDuration));
                    _newRotation = Quaternion.Euler(currentAngle.x + 90f, currentAngle.y + 0f, currentAngle.z + 0f);
                    break;
                case RotationMode.Top:
                    Debug.Log("top");
                    //iTween.RotateTo(gameObject, iTween.Hash("x", -90f)/* + Vector3.one * 360f, rotationFaceDuration);
                    //   iTween.RotateTo(gameObject, iTween.Hash("x", currentAngle.x -90f, "time", rotationFaceDuration));
                    _newRotation =(new Quaternion(0.7f, 0.7f, 0f,0f));
                    break;
                default:
                    return;
            }*/
            //According to : http://www.euclideanspace.com/maths/geometry/rotations/conversions/eulerToQuaternion/steps/
            switch (a_targetFace.rotationMode)
            {
                case RotationMode.None:
                    return;

                case RotationMode.R0_0_0: // OK
                    _newRotation = Quaternion.identity;
                    break;
                case RotationMode.R0_0_90:
                    _newRotation = new Quaternion(0, 0.7071f, 0, 0.7071f);
                    break;
                case RotationMode.R180_0_180: // OK
                    _newRotation = new Quaternion(0, 1f, 0f, 0);
                    break;
                case RotationMode.R0_270_0:
                    _newRotation = new Quaternion(0, -0.7071f, 0, 0.7071f);
                    break;

                case RotationMode.R0_90_0:
                    _newRotation = new Quaternion(0, 0f, 0.7071f, 0.7071f);
                    break;
                case RotationMode.R0_90_90:
                    _newRotation = new Quaternion(0.5f, 0.5f, 0.5f, 0.5f);
                    break;
                case RotationMode.R180_0_270: // OK
                    _newRotation = new Quaternion(0.7071f, 0.7071f, 0, 0);
                    break;
                case RotationMode.R0_90_270:
                    _newRotation = new Quaternion(-0.5f, -0.5f, 0.5f, 0.5f);
                    break;

                case RotationMode.R0_0_270: // OK
                    _newRotation = new Quaternion(0, 0, -0.7071f, 0.7071f);
                    break;
                case RotationMode.R0_270_90:
                    _newRotation = new Quaternion(-0.5f, 0.5f, -0.5f, 0.5f);
                    break;
                case RotationMode.R0_180_270: // OK
                    _newRotation = new Quaternion(-0.7071f, 0.7071f, 0, 0);
                    break;
                case RotationMode.R0_270_270:
                    _newRotation = new Quaternion(0.5f, -0.5f, -0.5f, 0.5f);
                    break;

                case RotationMode.R90_0_0:
                    _newRotation = new Quaternion(0.7071f, 0f, 0, 0.7071f);
                    break;
                case RotationMode.R90_0_270: // OK
                    _newRotation = new Quaternion(0.5f, 0.5f, -0.5f, 0.5f);
                    break;
                case RotationMode.R90_0_180:
                    _newRotation = new Quaternion(0, 0.7071f, -0.7071f, 0);
                    break;
                case RotationMode.R90_0_90:
                    _newRotation = new Quaternion(0.5f, -0.5f, 0.5f, 0.5f);
                    break;

                case RotationMode.R180_0_0:
                    _newRotation = new Quaternion(1, 0, 0, 0);
                    break;
                case RotationMode.R180_0_90:
                    _newRotation = new Quaternion(0.7071f, 0, -0.7071f, 0);
                    break;
                case RotationMode.R0_0_180:
                    _newRotation = new Quaternion(0, 0, 1f, 0);
                    break;
                case RotationMode.R0_90_180:
                    _newRotation = new Quaternion(0.7071f, 0, 0.7071f, 0);
                    break;

                case RotationMode.R270_0_0: // OK
                    _newRotation = new Quaternion(-0.7071f, 0f, 0, 0.7071f);
                    break;
                case RotationMode.R270_0_90: // OK
                    _newRotation = new Quaternion(-0.5f, 0.5f, 0.5f, 0.5f);
                    break;
                case RotationMode.R270_0_180:
                    _newRotation = new Quaternion(0, 0.7071f, 0.7071f, 0);
                    break;
                case RotationMode.R270_0_270: // OK
                    _newRotation = new Quaternion(-0.5f, -0.5f, -0.5f, 0.5f);
                    break;
            }
            _previousRotation = transform.rotation;
            Debug.Log(_previousRotation);
            Debug.Log(_newRotation);
            iTween.ValueTo(gameObject, iTween.Hash("from", 0f, "to", 1.0f, "time", rotationFaceDuration, "onupdate", "TestUpdate"));

            ChangeFaceActive(a_targetFace.face);
        }
        /*********************************************************/

        void TestUpdate(float a_t)
        {
            Debug.Log(a_t);
            transform.rotation = Quaternion.Lerp(_previousRotation, _newRotation, a_t);
            Debug.Log(transform.rotation);
            Debug.Log(transform.rotation.eulerAngles);
        }
    }
}
