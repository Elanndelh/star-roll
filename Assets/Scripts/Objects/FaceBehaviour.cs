﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LowRezJam2017
{
    //public enum RotationMode { None, Left, Right, Bottom, Top }
    public enum RotationMode { None, R0_0_0, R90_0_0, R180_0_0, R270_0_0,
        R0_90_0, R0_90_90, R0_90_180, R0_90_270,
        R0_270_0, R0_270_90, R0_180_270, R0_270_270,
        R0_0_90, R90_0_90, R180_0_90, R270_0_90,
        R0_0_180, R90_0_180, R180_0_180, R270_0_180,
        R0_0_270, R90_0_270, R180_0_270, R270_0_270,
    }


    [System.Serializable]
    public class TargetFace
    {
        public RotationMode rotationMode;
        public FaceBehaviour face;
    }


    public class FaceBehaviour : MonoBehaviour
    {
        public GameObject player;

        [SerializeField] [Tooltip("Those gameObjects are walls.")]
        List<GameObject> _walls;
        
        [SerializeField] [Tooltip("The first object needs to be the beginning of path, the last needs to be the end of path and other needs to be added in order.")]
        List<GameObject> _pathGameObject;
        int _currentPathIndex;

        [SerializeField] [Tooltip("The rotation mode when the player goes in the previous Face")]
        TargetFace _targetFaceBegin;
        [SerializeField] [Tooltip("The rotation mode when the player goes in the next Face.")]
        TargetFace _targetFaceEnd;


        CubeManager _cubeManager;

        
    ///////////////////////////////////////////////////////////////
    /// GENERAL FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        void Awake()
        {
            Debug.Assert(player != null, gameObject.name + " : player cannot be null");
            Debug.Assert(_walls != null, gameObject.name + " : _walls cannot be null");
            Debug.Assert(_walls != null, gameObject.name + " : _pathGameObject cannot be null");

            player.SetActive(false);
        }
        /*********************************************************/

        void Start()
        {
            _cubeManager = CubeManager.Instance;
            _currentPathIndex = 0;
        }
        /*********************************************************/

        void Update()
        {

        }
        /*********************************************************/
        
    ///////////////////////////////////////////////////////////////
    /// PRIVATE FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////
    /// PUBLIC FUNCTIONS //////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        public GameObject MoveForward()
        {
            if (_currentPathIndex < _pathGameObject.Count - 1)
            {
                _currentPathIndex++;
                return _pathGameObject[_currentPathIndex];
            }
            else if (_targetFaceEnd.face != null)
            {
                _cubeManager.ChangeFace(_targetFaceEnd);
            }
            return null;
        }
        /*********************************************************/

        public GameObject MoveBack()
        {
            if (_currentPathIndex > 0)
            {
                _currentPathIndex--;
                return _pathGameObject[_currentPathIndex];
            }
            else if (_targetFaceBegin.face != null)
            {
                _cubeManager.ChangeFace(_targetFaceBegin);
            }
            return null;
        }
        /*********************************************************/

        public void SetFrontMaterial()
        {
            foreach(GameObject wall in _walls)
            {
                if (wall.GetComponent<Renderer>() != null)
                    wall.GetComponent<WallBehavior>().SetFront(true);
            }
        }
        /*********************************************************/

        public void SetBackgroundMaterial()
        {
            foreach (GameObject wall in _walls)
            {
                if (wall.GetComponent<Renderer>() != null)
                    wall.GetComponent<WallBehavior>().SetFront(false);
            }
        }
        /*********************************************************/
    }
}
