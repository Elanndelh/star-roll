﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace LowRezJam2017
{
    /// <summary>
    /// Manager of a level
    /// </summary>
    public class LevelManager : MonoBehaviour
    {
        [SerializeField] [Tooltip("The GameOver's gameObject")]
        GameObject _gameOverGO;
        [SerializeField] [Tooltip("The YouWin's gameObject")]
        GameObject _youWinGO;
        [SerializeField] [Tooltip("The TitleImage what is the first thing the player will see. Don't need one if it's not the final level.")]
        GameObject _titleImage;
        //1/[SerializeField] [Tooltip("The reloadLevel button to restart level when the player fails.")]
        //1/GameObject _reloadButtonGO; // Not used
        [SerializeField] [Tooltip("Name of the next level if this one is successed.")]
        string _nextLevelName;

        public static LevelManager Instance;

        bool _isGameOver;
        bool _isPaused;
        public bool IsPaused() { return _isPaused; }
        public void SetPaused(bool a_bool) { _isPaused = a_bool; }

        Animator _animator;


    ///////////////////////////////////////////////////////////////
    /// GENERAL FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        void Awake()
        {
            Instance = this;
            Debug.Assert(_gameOverGO != null, " _gameOverGO cannot be null");
            Debug.Assert(_titleImage != null, " _titleImage cannot be null");
            //1/Debug.Assert(_reloadButtonGO != null, " _reloadButtonGO cannot be null");
        }
        /*********************************************************/

        void Start() {
            _isPaused = true;
            _isGameOver = false;
            _titleImage.SetActive(true);
            //1/_reloadButtonGO.SetActive(false);

            _animator = GetComponent<Animator>();
            if (_animator == null)
                Debug.LogError(gameObject.name + " : _animator is null.");

            StartCoroutine(InitializeLevelCoroutine());
        }
        /*********************************************************/

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                // Quit game and return to main menu
                StartCoroutine(QuitLevelCoroutine("MainMenu"));
            }
            if (_isGameOver)
            {
                if (Input.GetMouseButtonDown(0))
                    Btn_ReloadLevel();
            }
        }
        /*********************************************************/

    ///////////////////////////////////////////////////////////////
    /// GENERAL FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        /// <summary>
        /// Coroutine: launch when the level begins
        /// </summary>
        IEnumerator InitializeLevelCoroutine()
        {
            yield return new WaitForSeconds(1.5f); // Length of Hide_TitleImage (clip)
            _isPaused = false;
        }
        /*********************************************************/

        /// <summary>
        /// Coroutine: change current scene after a animation
        /// </summary>
        /// <param name="a_levelName"></param>
        IEnumerator QuitLevelCoroutine(string a_levelName)
        {
            _animator.SetTrigger("Quit");
            yield return new WaitForSeconds(1.0f); // Length of FadeOut (clip)
            SceneManager.LoadScene(a_levelName);
        }
        /*********************************************************/
        
    ///////////////////////////////////////////////////////////////
    /// PUBLIC FUNCTIONS //////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        /// <summary>
        /// Called to gameOver the level
        /// </summary>
        public void GameOver()
        {
            _gameOverGO.SetActive(true);
            //1/_reloadButtonGO.SetActive(true);
            _isPaused = true;
            _isGameOver = true;
        }
        /*********************************************************/

        /// <summary>
        /// Called to load the next level after eventually appearance of the YouWin's gameObject (if it's the final level)
        /// </summary>
        public void LoadNextLevel()
        {
            if (_nextLevelName == "EndScene")
            {
                if (_youWinGO != null)
                    _youWinGO.SetActive(true);
                else
                    Debug.LogError("_youWinGO is null.");
            }

            if (string.IsNullOrEmpty(_nextLevelName) == false)
                StartCoroutine(QuitLevelCoroutine(_nextLevelName));
        }
        /*********************************************************/

        /// <summary>
        /// Called by _reloadButtonGO
        /// Reload the current level
        /// </summary>
        ///<statut>Not called (bug of button's interactability ?)</statut>
        public void Btn_ReloadLevel()
        {
            Debug.Log(SceneManager.GetActiveScene().name);
            StartCoroutine(QuitLevelCoroutine(SceneManager.GetActiveScene().name));
        }
        /*********************************************************/
    }
}
