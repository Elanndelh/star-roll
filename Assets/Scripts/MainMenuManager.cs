﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace LowRezJam2017
{
    public class MainMenuManager : MonoBehaviour
    {
        Animator _animator;


    ///////////////////////////////////////////////////////////////
    /// GENERAL FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        void Start()
        {
            _animator = GetComponent<Animator>();
            if (_animator == null)
                Debug.LogError(gameObject.name + " : _animator is null.");
        }
        /*********************************************************/

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }
        }
        /*********************************************************/
        
    ///////////////////////////////////////////////////////////////
    /// PRIVATE FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        /// <summary>
        /// Coroutine: play a animation then change current scene
        /// </summary>
        IEnumerator QuitCoroutine(string a_levelName)
        {
            _animator.SetTrigger("Quit");
            yield return new WaitForSeconds(1.0f); // Length of FadeOut (clip)
            SceneManager.LoadScene(a_levelName);
        }
        /*********************************************************/

    ///////////////////////////////////////////////////////////////
    /// PUBLIC FUNCTIONS //////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        /// <summary>
        /// Called by StartButton in MainMenu
        /// Launch a coroutine to start the game
        /// </summary>
        public void Btn_LaunchGame()
        {
            StartCoroutine(QuitCoroutine("Intro"));
        }
        /*********************************************************/
    }
}
