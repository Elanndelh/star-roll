﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace LowRezJam2017
{
    public class EndManager : MonoBehaviour
    {
        [SerializeField] [Tooltip("Duration before the credit gameObject appears")]
        float _durationBeforeCredit;
        [SerializeField] [Tooltip("The credit gameObject")]
        GameObject _creditImageGO;

        [SerializeField] [Tooltip("Duration before the game returns to MainMenu, after credit gameObject appears")]
        float _durationBeforeMainMenu;

        
    ///////////////////////////////////////////////////////////////
    /// GENERAL FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        void Awake()
        {
            _creditImageGO.SetActive(false);
        }
        /*********************************************************/

        void Start()
        {
            StartCoroutine(EndCoroutine());
        }
        /*********************************************************/
        
    ///////////////////////////////////////////////////////////////
    /// PRIVATE FUNCTIONS /////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
        /// <summary>
        /// Coroutine: Determine order of the end "animation".
        /// </summary>
        IEnumerator EndCoroutine()
        {
            yield return new WaitForSeconds(_durationBeforeCredit);
            _creditImageGO.SetActive(true);
            yield return new WaitForSeconds(_durationBeforeMainMenu);
            SceneManager.LoadScene("MainMenu");
        }
        /*********************************************************/
        
    }
}
